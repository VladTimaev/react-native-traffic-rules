import { createDrawerNavigator } from '@react-navigation/drawer';
import Ionicons from "react-native-vector-icons/Ionicons";



/////////////  Components  ////////////////////////

import HomeScreen from './pages/HomeScreen';
import RegionScreen from './pages/RegionScreen';
import PhoneScreen from './pages/PhoneScreen';
import Settings from './pages/SettingsScreen';

/////////////  Components  ////////////////////////



export default function Router () {

    const Drawer = createDrawerNavigator();
   
   
    return (
    <> 
       <Drawer.Navigator
         drawerType="front" 
         screenOptions={{
          headerStyle: {
            backgroundColor: '#3AB4F2',},
            headerTintColor: '#fff', 
            headerTitleStyle: {
            fontWeight: 'bold',
            },
         }}
        >
      <Drawer.Screen name="Главная" component={HomeScreen}
        options= {{
        drawerIcon: ({focused, size}) => (
            <Ionicons
               name="cube-outline"
               size={size}
               color={focused ? '#7cc' : '#ccc'}
            />
          ),
        }}
        />
        <Drawer.Screen name="Коды регионов" component={RegionScreen} 
        options= {{
          drawerIcon: ({focused, size}) => (
            <Ionicons
               name="cube-outline"
               size={size}
               color={focused ? '#7cc' : '#ccc'}
            />
          ),
        }}
        />
        <Drawer.Screen name="Телефоны доверия" component={PhoneScreen}
        options= {{
          drawerIcon: ({focused, size}) => (
            <Ionicons
               name="book"
               size={size}
               color={focused ? '#7cc' : '#ccc'}
            />
          ),
        }}
         />
        <Drawer.Screen name="Настройки" component={Settings} 
        options= {{
          drawerIcon: ({focused, size}) => (
            <Ionicons
               name="settings-outline"
               size={size}
               color={focused ? '#7cc' : '#ccc'}
            />
          ),
        }}
        />
   
     </Drawer.Navigator>
    </>
    )
}
