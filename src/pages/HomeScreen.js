import {View, Button} from "react-native";
import Slider from "../components/Slider";

export default function HomeScreen () {
  return (
    <View>
      <Slider />
    </View>
  );
}
