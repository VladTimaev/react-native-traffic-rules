import Tickets from "../components/Tickets";
import { Button } from "react-native";


export default function TicketScreen ({navigation}) {

  return (
    <>
    <Tickets />
    <Button onPress={() => navigation.goBack()} 
    title="Назад" />
    </>
  );
}
