import { StyleSheet, Text, View, VirtualizedList, StatusBar, SafeAreaView } from 'react-native';
import data from "../../assets/temp/phones_number.json";

export default function PhoneScreen () {

 
  const getItem = (data, index) => {
    return data[index]
  };
  
  const Item = (item) => ( 
     <View style={styles.item}>
     <Text style={styles.titles}>{item.title}</Text>
     <Text style={styles.title}>{item.value}</Text>
     </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <VirtualizedList
        data={data}
        initialNumToRender={4}
        renderItem={({ item }) => <Item title={item.title} key={item.id} value={item.value}/>}
        keyExtractor={item => item.key}
        getItemCount={data => data.length}
        getItem={getItem}
      />
      
    </SafeAreaView>
    
  );
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: StatusBar.currentHeight,
    },
    item: {
      height: 70,
      marginVertical: 0,
      marginHorizontal: 0,
      padding: 15,
    },
    titles: {
     color: "#fff",
     fontSize: 16,
     margin: 0,
     padding: 5,
     lineHeight: 30,
     backgroundColor: '#3AB0FF',
     borderRadius: 5
    },
    title: {
      fontSize: 20,
      backgroundColor: "#fff",
      padding: 5,
      height: 100,
      borderRadius: 5
    },
  });


