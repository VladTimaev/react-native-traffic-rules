import { Button, StyleSheet, Text, View, VirtualizedList, StatusBar, SafeAreaView, Image } from "react-native";
import data from "../../assets/data/modal";



export default function RegionScreen ({ navigation }) {

  return (
    <View>
     <FloatList />
     <Button onPress={() => navigation.goBack()} title="Назад" />
    </View>
  );
}

function FloatList () {

  const getItem = (data, index) => {
    return data[index]
  };

  const Item = (item) => (
      <View style={styles.item}>
        <Text style={styles.titles}>{item.title}
          <Image
              style={styles.itemImage}
              source={item.image }/>
        </Text>

      </View>
  );

  return (
      <SafeAreaView style={styles.container}>
        <VirtualizedList
            data={data}
            initialNumToRender={4}
            renderItem={({ item }) => <Item title={item.title} key={item.id} image={item.image}/>}
            keyExtractor={item => item.key}
            getItemCount={data => data.length}
            getItem={getItem}
        />
      </SafeAreaView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
  },
  itemImage: {
    height: 20,
    width: 20,
  },
  item: {
    height: 70,
    marginVertical: 0,
    marginHorizontal: 0,
    padding: 15,
  },
  titles: {
    color: "#fff",
    fontSize: 16,
    margin: 0,
    padding: 5,
    lineHeight: 30,
    backgroundColor: '#3AB0FF',
    borderRadius: 5
  },
  title: {
    fontSize: 20,
    backgroundColor: "#fff",
    padding: 5,
    height: 100,
    borderRadius: 5
  },
});