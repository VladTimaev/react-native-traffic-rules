import { Button } from 'react-native';
import RulesList from "../components/RulesList";


export default function ProfileScreen ({ navigation }) {

  return (
    <>
    <RulesList />
    <Button onPress={() => navigation.goBack()} title="Назад" />
    </>
  );
}
