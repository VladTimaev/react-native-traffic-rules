import RoadSigns from "../components/RoadSigns";
import { Button } from "react-native";


export default function RoadSignsScreen ({navigation}) {

  return (
    <>
    <RoadSigns />
    <Button onPress={() => navigation.goBack()} 
    title="Назад" />
    </>
  );
}
