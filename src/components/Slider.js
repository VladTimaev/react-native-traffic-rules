import React, { useState } from 'react';
import AppIntroSlider from 'react-native-app-intro-slider';
import slides from "../../assets/data/slider";
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView, StyleSheet, View, Text, Image, Button, TouchableOpacity  } from 'react-native';

// import all the components we are going to use

export default function Slider () {
  
  const [showRealApp, setShowRealApp] = useState(false);

  const navigation = useNavigation();

  const onDone = () => {
    setShowRealApp(true);
  };

  const onSkip = () => {
    setShowRealApp(true);
  };

  const RenderItem = ({ item }) => {
    return (
       <View
        style={{
          flex: 1,
          backgroundColor: item.backgroundColor,
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingBottom: 80,
        }}>
        <Text style={styles.introTextStyle}>{item.title}</Text> 
        <TouchableOpacity  onPress={() => {navigation.navigate(item.navigator)}}>
        <View style={styles.containerCircle}>
        <Image 
        style={styles.introImageStyle} 
        source={item.image} 
        />
        </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <>
      {showRealApp ? (
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <Text style={styles.titleStyle}>
              React Native App Intro Slider using AppIntroSlider
            </Text>
            <Text style={styles.paragraphStyle}>
              This will be your screen when you click Skip from any slide or
              Done button at last
            </Text>
            <Button
              title="Show Intro Slider again"
              onPress={() => setShowRealApp(false)}
            />
          </View>
        </SafeAreaView>
      ) : (
        <AppIntroSlider
          data={slides}
          renderItem={RenderItem}
          onDone={onDone}
          showSkipButton={true}
          onSkip={onSkip}
        />
      )}
    </>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 10,
    justifyContent: 'center',
  },
  containerCircle: {
    height: 200,
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 400,
    border: "solid 20px #3AB4F2"
  },
  titleStyle: {
    padding: 10,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  paragraphStyle: {
    padding: 20,
    textAlign: 'center',
    fontSize: 16,
  },
  introImageStyle: {
    width: 160,
    height: 160,
    borderRadius: 500
  },
  introTextStyle: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
    paddingVertical: 30,
  },
  introTitleStyle: {
    fontSize: 25,
    color: 'white',
    textAlign: 'center',
    marginBottom: 16,
    fontWeight: 'bold',
  },
});

