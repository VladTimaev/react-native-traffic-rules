import { StyleSheet, Text, View, TouchableOpacity, VirtualizedList, StatusBar, SafeAreaView } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import data from "../../assets/temp/road_signs.json";
import { useState } from "react";
import {useNavigation} from "@react-navigation/native";


export default function FloatList () {

  const navigation = useNavigation();
  const [items, setItems] = useState(data);
  const getItem = (data, index) => {
    return data[index]
  };
  
  const Item = (item) => ( 
    <TouchableOpacity onPress={() => navigation.navigate("")}>
     <View style={styles.item}>
      <Text style={styles.titles}>{item.title}
      <Ionicons name="chevron-forward-outline" size="20" />
      </Text>
     </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={styles.container}>
      <VirtualizedList
        data={items}
        initialNumToRender={4}
        renderItem={({ item }) => <Item title={item.title} key={item.id} />}
        keyExtractor={item => item.key}
        getItemCount={data => data.length}
        getItem={getItem}
      />
    </SafeAreaView>
  );
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: StatusBar.currentHeight,
    },
    item: {
      height: 70,
      marginVertical: 0,
      marginHorizontal: 0,
      padding: 15,
    },
    titles: {
     color: "#fff",
     fontSize: 16,
     margin: 0,
     padding: 5,
     lineHeight: 30,
     backgroundColor: '#3AB0FF',
     borderRadius: 5
    },
    title: {
      fontSize: 20,
      backgroundColor: "#fff",
      padding: 5,
      height: 100,
      borderRadius: 5
    },
  });