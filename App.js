import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Navigation from "./assets/data/navigation";

export default function App() {

    const SliderStack = createNativeStackNavigator();

    return (
        <>
            <NavigationContainer>
                <SliderStack.Navigator
                    screenOptions={{
                        headerStyle: {
                            backgroundColor: '#3AB4F2',},
                        headerTintColor: '#fff',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },}}
                >
                    {Navigation()}
                </SliderStack.Navigator>
            </NavigationContainer>
        </>
    );
}
