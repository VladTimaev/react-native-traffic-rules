export default [
    {
        key: 1,
        title: 'Бишкек — 01',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
      {
        key: 2,
        title: 'Ош — 02',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
      {
        key: 3,
        title: 'Баткенская область — 03',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
      {
        key: 4,
        title: 'Джалал-Абадская область — 04',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
      {
        key: 5,
        title: 'Нарынская область — 05',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
      {
        key: 6,
        title: 'Ошская область — 06',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
      {
        key: 7,
        title: 'Таласская область — 07',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
      {
        key: 8,
        title: 'Чуйская область — 08',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
      {
        key: 9,
        title: 'Иссык-Кульская область — 09',
        image: {
          uri: require("../../assets/icons/img/icon-kg.png")
        },
      },
];

