import React from "react";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {getFocusedRouteNameFromRoute} from "@react-navigation/native";


import Router from "../../src/Router";
import HomeScreen from "../../src/pages/HomeScreen";
import ProfileScreen from "../../src/pages/ProfileScreen";
import RoadSignsScreen from "../../src/pages/RoadSignsScreen";
import TicketScreen from "../../src/pages/TicketScreen";
import ActionsScreen from "../../src/pages/ActionsScreen";
import RegionScreen from "../../src/pages/RegionScreen";


function getHeaderTitle(route) {

    const routeName = getFocusedRouteNameFromRoute(route) ?? 'Правила дорожного движения';

    switch (routeName) {
        case 'Правила дорожного движения':
            return 'дорожного';
    }
}

export default function Navigation () {
    const SliderStack = createNativeStackNavigator();

    return (
        <SliderStack.Group>
            <SliderStack.Screen name='Home' component={Router} options={{headerShown: false}}    />
            <SliderStack.Screen
                name="Главная"
                component={HomeScreen}
                options={({ route }) => ({
                    headerTitle: getHeaderTitle(route),
                })}
            />
            <SliderStack.Screen name="Правила дорожного движения" component={ProfileScreen} />
            <SliderStack.Screen name="Дорожные знаки и разметка" component={RoadSignsScreen}/>
            <SliderStack.Screen name="Билеты для экзамена" component={TicketScreen} />
            <SliderStack.Screen name="Действия при ДТП" component={ActionsScreen} />
            <SliderStack.Screen name="Коды регионов" component={RegionScreen} />
        </SliderStack.Group>
    )
}