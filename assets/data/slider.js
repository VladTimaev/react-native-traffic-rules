export default [
  {
    key: 1,
    title: 'Правила дорожного движения',
    navigator: "Правила дорожного движения",
    image: {
      uri: require("../../assets/icons/img/pdd.png"),
    },
    backgroundColor: '#20d2bb',
  },
  {
    key: 2,
    title: 'Дорожные знаки и разметка',
    navigator: "Дорожные знаки и разметка",
    image: {
      uri: require("../../assets/icons/img/road-signs.png"),
    },
    backgroundColor: '#febe29',
  },
  {
    key: 3,
    title: 'Билеты для экзамена',
    navigator: "Билеты для экзамена",
    image: {
      uri: require("../../assets/icons/img/colored.png"),
    },
    backgroundColor: '#22bcb5',
  },
  {
    key: 4,
    title: 'Действия при ДТП',
    navigator: "Действия при ДТП",
    image: {
      uri: require("../../assets/icons/img/accident.png"),
    },
    backgroundColor: '#3395ff',
  },
  {
    key: 5,
    title: 'Коды регионов на автономерах',
    navigator: "Коды регионов",
    image: {
      uri: require("../../assets/icons/img/city-street.png"),
    },
    backgroundColor: '#f6437b',
  },
];